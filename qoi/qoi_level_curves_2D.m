% Targets learning a specific level curve of a function in 2 variables.
 
function ret_val = qoi_level_curves_2D( mu , level, tol)

ret_val = 1.0*(abs(mu - level) < tol);

end

