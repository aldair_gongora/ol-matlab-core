% Single Target QOI:
%   min_x abs(f(x) - target)

function ret_val = qoi_target( mu , target)
    ret_val = max(-abs((mu-target)/target));
end

