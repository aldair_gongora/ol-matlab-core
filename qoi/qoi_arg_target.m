% Single Target QOI:
%   argmin_x abs(f(x) - target)

function ret_val = qoi_arg_target( mu , target)
    [~, ret_val] = max(-abs((mu-target)/target));
end

