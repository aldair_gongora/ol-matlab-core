
function  ret_val = qoi_inverse_poly(mu, n, X)
    XX = X';
    Phi = zeros(length(X), n+1);
    for i=0:n
        Phi(:, i+1) = XX.^(n-i);
    end
    ret_val = Phi\mu; 
end

