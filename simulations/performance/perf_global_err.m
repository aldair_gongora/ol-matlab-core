% Global relative error using L2 norm
function ret_val = perf_global_err( mu, truth )
    ret_val = norm(mu-truth)/norm(truth);
end

