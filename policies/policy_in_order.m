% Measures in order in uniform grid with repitition
function ret_val = policy_in_order(~, mu, ~, ~, n, step_size)

% determine which index gets scored this round
idx = mod(round(n*step_size), length(mu)) + 1;
ret_val = zeros(size(mu));
ret_val(idx) = 1;
 
end

