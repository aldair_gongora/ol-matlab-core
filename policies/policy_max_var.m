function ret_val = policy_max_var(i, ~, Sigma0, ~, ~)
    d = diag(Sigma0);
    ret_val = d(i);
end

